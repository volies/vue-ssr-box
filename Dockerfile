FROM node

RUN mkdir -p /root/app
COPY . /root/app

WORKDIR /root/app

RUN npm install

ENV NODE_ENV=production

RUN npm run build

EXPOSE 80

CMD ["npm", "start"]
