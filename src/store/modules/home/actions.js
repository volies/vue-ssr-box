import axios from 'axios';

export default {
  send({commit}) {
    return axios('https://jsonplaceholder.typicode.com/users')
        .then((response) => {
          const data = response.data;

          commit('SET_FRIENDS', data);
        });
  }
}
