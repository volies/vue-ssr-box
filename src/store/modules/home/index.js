import actions from "./actions";
import mutations from "./mutations";
import getters from "./getters";

export default {
    namespaced: true,
    state: {
        friends: [{
            id: 1,
            name: "ivan"
        }]
    },
    actions,
    mutations,
    getters
}
